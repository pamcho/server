import Socket

@available(macOS 10.15.4, *)
@main
public struct Server {
    public static func main() {
        var continueRunning: Bool = true
        
        let fileReader: FileReader = .init()

        do {
            let socket = try Socket.create(family: .inet)
            socket.readBufferSize = 4096
            try socket.listen(on: 5543)
            print("Listening on port: \(socket.listeningPort)...\n")
            
            while continueRunning {
                
                let newSocket = try socket.acceptClientConnection()
                print("Connection established")
                
                if let levelRequest = try newSocket.readString() {
                    print("Message received: \(levelRequest)")
                    
                    if levelRequest.hasPrefix("level7") || levelRequest.hasPrefix("level8") || levelRequest.hasPrefix("level9") {
                        
                        var levelContent: String
                        do {
                            levelContent = try fileReader.loadLevelFromFile(levelRequest)
                        } catch {
                            levelContent = "Wrong path or file name"
                        }
                        try newSocket.write(from: levelContent)
                        print("Message was sent\n")
                        
                    } else if levelRequest.hasPrefix("QUIT") {
                        continueRunning = false
                    } else {
                        print("Wrong request")
                    }
                }
                newSocket.close()
            }
            
            socket.close()
        } catch {
            print("Unable to run")
        }

    }
}
